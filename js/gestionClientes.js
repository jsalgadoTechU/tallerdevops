var clientesObtenidos;

function getClientes() {
  var url ="http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange=function()
  {
    if (this.readyState == 4 && this.status == 200)
    {
        console.log(request.responseText);
        clientesObtenidos = request.responseText;
        procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}




function procesarClientes()
{
  var JSONClientes = JSON.parse(clientesObtenidos);
  var tabla = document.getElementById("tablaClientes");

  for (var i = 0; i < JSONClientes.value.length; i++) {

    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");

    var imagen = document.createElement("img");
    imagen.classList.add("flag");

    columnaNombre.innerText = JSONClientes.value[i].ContactName;
    var columnaDireccion = document.createElement("td");
    columnaDireccion.innerText = JSONClientes.value[i].Address;
    var columnaPais = document.createElement("td");

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaDireccion);

    if (JSONClientes.value[i].Country=="UK")
    {
      imagen.setAttribute("src", "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+"United-Kingdom.png")
    }
    else {
    imagen.setAttribute("src", "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONClientes.value[i].Country+".png")
  }
    columnaPais.appendChild(imagen);
    nuevaFila.appendChild(columnaPais);
    //columnaPais.innerText = JSONClientes.value[i].Country;

    tabla.appendChild(nuevaFila);
    //console.log(JSONClientes.value[i].ProductName);

  }
}
